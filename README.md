# ArgoJs - 3D models

## Installation

- Download Git repository

- Put assets, css, js and models folder into your web-hosting

- Add 
```html
    <link rel="stylesheet" href="css/agrojs_main.css">
```
into the head section. This paragraph may differ in some cases. The main idea is to add agrojs_main.css to your website

- Add 
```html
    <script src="js/qrcode.min.js"></script>
```
into the footer section. This paragraph may differ in some cases. The main idea is to add qrcode.min.js to your website

- Add 
```html
    <script src="js/arinsert.js"></script>
```
into the footer section. This paragraph may differ in some cases. The main idea is to add arinsert.js to your website

- Define variable argoBaseUrl with URL name that provides models. For example, your models are stored on domain argo-models.com, so define variable with https://argo-models.com/. Be carefull, because folder "models" have to be available from root path, for example in this case it will be https://argo-models.com/models/. Script example

```html
    <script>
        var argoBaseUrl = 'https://ar2qr.com/';
    </script>
```

- Create folder with model name you will use on your website in "models" folder. For example you wanna add new model with name "sofa" to your website, so create folder "sofa" in "models" folder, then add to folder "sofa" 3 files:
    - model.glb
    - model.usdz
    - static.png
    
- After that your path will looks like:
    - models/sofa/model.glb
    - models/sofa/model.usdz
    - models/sofa/static.png
    
- After that add class name "argo-item" to the element you want to dispatch 3D modal

- Add data attribute "data-model-name" with name of the model. For example if you've created sofa folder, so add data-model-name="sofa"

- Optional: if you want to put your own QR code, you can define data-qr-url with path to QR code image. For example if you store QR codes in "assets" folder you can fill attribute with "assets/qrcodename.jpg"

- You can find demo in index.html file
