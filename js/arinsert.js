function getMobileOS() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/windows phone/i.test(userAgent)) return "Windows Phone";
    if (/android/i.test(userAgent)) return "Android";
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) return "iOS";
    if (/webOS|BlackBerry|IEMobile|Opera Mini/i.test(userAgent)) return "Other";
    return "Uncertain";
}

function insertModel(mname, targetModel) {

    console.log("THIS")

    let bg = document.createElement('DIV');
    let h = location.protocol;
    bg.setAttribute('id', 'ar-bg');
    bg.style = 'background-color:rgba(250,250,250,0.3);backdrop-filter:blur(4px);position:fixed;left:0;top:0;z-index:999999;width:100%;height:100%';
    bg.addEventListener('click', (e) => { let b = document.getElementById('ar-bg'); if (e.target == b) document.body.removeChild(b) });
    let x = document.createElement('DIV');
    x.setAttribute('id', 'ar-model');
    x.style = 'position:relative;width:50vh;max-width:40vw;height:50vh;max-height:40vw;margin:30vh auto';
    x.innerHTML = '<model-viewer style="width:100%;height:100%;border: 2px solid #bbb;box-shadow:12px 12px 2px 1px rgba(50,50,50,.3);background:rgba(250,250,250,.6)" src="' + argoBaseUrl + mname + '/model.glb" ios-src="' + h + '//ar2qr.com/models/' + mname + '/model.usdz?v=1569545377878" alt="A 3D model" shadow-intensity="1" camera-controls auto-rotate ar></model-viewer>';
    bg.appendChild(x);
    if (getMobileOS() == "Uncertain") {
        let y = document.createElement('IMG');
        x.setAttribute('id', 'ar-logo');
        y.style = 'position:absolute;right:3px;bottom:3px;display:block';
        y.src = 'assets/ar1c.png';
        y.addEventListener('click', (e) => {
            let b = document.getElementById('ar-logo');
            if (b) {

                while (b.hasChildNodes()) { b.removeChild(b.lastChild) };

                if (targetModel.getAttribute('data-qr-url')) {

                    const customQrCodeUrl = targetModel.getAttribute('data-qr-url')
                    const customQrCode = document.createElement('img')
                    customQrCode.src = customQrCodeUrl
                    b.appendChild(customQrCode)

                } else {
                    new QRCode(b, {
                        text: argoBaseUrl + mname,
                        width: b.offsetWidth - 2,
                        height: b.offsetHeight - 2,
                        colorDark: "#511",
                        colorLight: "#ffffff",
                        correctLevel: QRCode.CorrectLevel.H
                    });
                }
            }
        });
        x.appendChild(y);
    }
    document.body.appendChild(bg);
}

document.addEventListener("DOMContentLoaded", () => {
    var models = document.querySelectorAll('[data-model-name]');
    for (let m of models) {
        let mname = m.getAttribute('data-model-name');
        let modelIcon = document.createElement('IMG');
        modelIcon.src = 'assets/3d0view-icon.png';
        modelIcon.classList.add('argo-model-icon');

        m.addEventListener('click', (e) => { insertModel(mname, m) });
        m.appendChild(modelIcon);
    }
    if (typeof window.modelWiewerLoaded == 'undefined') {
        let s = document.createElement("SCRIPT");
        s.src = "https://unpkg.com/@google/model-viewer/dist/model-viewer.js";
        s.type = "module";
        document.body.appendChild(s);
        window.modelWiewerLoaded = true;
    }
    if (typeof QRCode == 'undefined') {
        // let s = document.createElement("SCRIPT");
        // s.src = "https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js";
        // document.body.appendChild(s);
    }
})
